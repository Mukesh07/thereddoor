package com.example.mukesh.splash.Utils;

/**
 * Created by Mukesh on 11/5/2015.
 */
public interface OnItemClickListener {
    void onItemClick(int position);
}
