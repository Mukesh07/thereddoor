package com.example.mukesh.splash.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.mukesh.splash.Utils.Constants;
import com.example.mukesh.splash.R;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

public class Splash extends AppCompatActivity implements Constants {
    Button tryAgain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new RetrieveTokenTask().execute();
        new RetrieveTokenTask().execute();

    }

    private class RetrieveTokenTask extends AsyncTask<Void, Void, String> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(Splash.this, "Progress Dialog", "Processing !");

        }

        @Override
        protected String doInBackground(Void... params) {

            String scopes = GCM;
            String Id = PROJECT;
            String token = null;
            try {
                token = InstanceID.getInstance(Splash.this).getToken(Id, scopes);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return token;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s != null && !s.isEmpty()) {
                //Toast.makeText(Splash.this, s, Toast.LENGTH_SHORT).show();
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Splash.this);
                SharedPreferences.Editor sharedEditor = sharedPreferences.edit();
                sharedEditor.putString(DEVICE_TOKEN, s.toString());
                sharedEditor.commit();
                dialog.dismiss();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        Intent mainIntent = new Intent(Splash.this, SignIn.class);
                        Splash.this.startActivity(mainIntent);
                        Splash.this.finish();
                    }
                }, SPLASH_DISPLAY_LENGTH);
            } else

            {

                Toast.makeText(Splash.this, MSG_NO_INTERNET_CONNECTION, Toast.LENGTH_SHORT).show();
            }
        }
    }
}

