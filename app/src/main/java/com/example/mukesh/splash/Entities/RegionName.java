package com.example.mukesh.splash.Entities;

/**
 * Created by Mukesh on 11/6/2015.
 */
public class RegionName {

        String regionName, _id,regionNickname,zip,__v;


        public String getRegionName() {
            return regionName;
        }

        public String get_id() {
            return _id;
        }

        public String getRegionNickname() {
            return regionNickname;
        }

        public String getZip() {
            return zip;
        }

        public String get__v() {
            return __v;
        }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }
}


