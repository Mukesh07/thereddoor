package com.example.mukesh.splash.Utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


public class ConvertLocationToLatLng implements Constants {
    public static void getLatLng(final String customerLocation, final Context context, final Handler handler) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                double[] latLng = new double[2];
                try {
                    List customerAddress = geocoder.getFromLocationName(customerLocation, 1);
                    if (customerAddress != null && customerAddress.size() > 0) {
                        Address address = (Address) customerAddress.get(0);
                        latLng[0] = address.getLatitude();
                        latLng[1] = address.getLongitude();

                    }
                } catch (IOException e) {
                    Log.e(EXCEPTION, GEOCODER_BAD_REQUEST, e);
                } finally {
                    Message message = Message.obtain();
                    message.setTarget(handler);
                    message.what = 1;
                    Bundle bundle = new Bundle();
                    bundle.putDoubleArray(LATLNG, latLng);
                    message.setData(bundle);
                    message.sendToTarget();
                }
            }
        };
        thread.start();
    }


    public static void getAddressDetails(final String address,final Context context,final Handler handler)
    {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                double[] latLng= new double[2];
                String zip="", city="", street="", state="", appartment="";
                try {
                    List customerAddress = geocoder.getFromLocationName(address, 1);
                    if (customerAddress != null && customerAddress.size() > 0) {
                        Address agentAddress = (Address) customerAddress.get(0);
                        zip = agentAddress.getPostalCode();
                        city =agentAddress.getLocality() ;
                        street =agentAddress.getFeatureName();
                        state =agentAddress.getAdminArea();
                        appartment =agentAddress.getSubLocality();
                        latLng[0] = agentAddress.getLatitude();
                        latLng[1] = agentAddress.getLongitude();


                    }
                } catch (IOException e) {
                    Log.e("Exception", "Unable to connect to Geocoder", e);
                } finally {
                    Message message = Message.obtain();
                    message.setTarget(handler);
                    message.what=1;
                    Bundle bundle = new Bundle();
                    bundle.putString(ZIP, zip);
                    bundle.putString(CITY, city);
                    bundle.putString(STATE, state);
                    bundle.putString(STREET, street);
                    bundle.putString(APPARTMENT, appartment);
                    bundle.putDoubleArray("AddressLatLng",latLng);
                    message.setData(bundle);
                    message.sendToTarget();
                }
            }
        };
        thread.start();
    }

}