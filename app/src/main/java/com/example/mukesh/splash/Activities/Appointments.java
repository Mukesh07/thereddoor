package com.example.mukesh.splash.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mukesh.splash.Entities.AppointmentDetails;
import com.example.mukesh.splash.Adapters.ListAdapterAppointments;
import com.example.mukesh.splash.Entities.LoginResponse;
import com.example.mukesh.splash.Retrofit.RestClient;
import com.example.mukesh.splash.Utils.Constants;
import com.example.mukesh.splash.Utils.OnItemClickListener;
import com.example.mukesh.splash.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class Appointments extends AppCompatActivity implements View.OnClickListener, OnItemClickListener,Constants, AdapterView.OnItemClickListener {
    DrawerLayout dLayout;
    ListView dList, list;
    ArrayAdapter navAdapter;
    ArrayList<AppointmentDetails> appointmentsArrayList = new ArrayList<>();
    ActionBarDrawerToggle drawerListener;
    ListAdapterAppointments listAdapter;
    RelativeLayout drawerRelative;
    Button scheduled,invitations;
    ImageButton back;
    SharedPreferences sharedPreferences;
    TextView name;
    public static Boolean seeInvitationState=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointments);
        TextView header = (TextView) findViewById(R.id.default_text);
        header.setText(APPOINTMENT);
        back = (ImageButton)findViewById(R.id.back);
        back.setImageResource(R.mipmap.menu_icon_normal);
        scheduled = (Button)findViewById(R.id.scheduled);
        invitations =(Button) findViewById(R.id.invitations);
        dList = (ListView) findViewById(R.id.left_drawer);
        list = (ListView) findViewById(R.id.list);
        name =(TextView) findViewById(R.id.name);
        drawerRelative = (RelativeLayout)findViewById(R.id.navDrawerRelative);
        listAdapter = new ListAdapterAppointments(this, appointmentsArrayList);
        navAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.menu));
        dList.setAdapter(navAdapter);
        list.setAdapter(listAdapter);
        dList.setOnItemClickListener(this);
        listAdapter.notifyDataSetChanged();
        createList();
        back.setOnClickListener(this);
        scheduled.setOnClickListener(this);
        invitations.setOnClickListener(this);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        dLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerListener = new ActionBarDrawerToggle(this, dLayout, R.string.opendrawer, R.string.closedrawer) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                String drawer= sharedPreferences.getString("firstName","");
                name.setText(drawer);

                super.onDrawerOpened(drawerView);
            }
        };
        dLayout.setDrawerListener(drawerListener);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                dLayout.openDrawer(drawerRelative);
                break;
            case R.id.scheduled:
                scheduled.setBackgroundResource(R.mipmap.small_btn_pressed);
                invitations.setBackgroundResource(R.mipmap.small_btn_normal);
                scheduled.setTextColor(Color.WHITE);
                invitations.setTextColor(Color.BLACK);
                seeInvitationState = false;
                listAdapter.notifyDataSetChanged();
                break;
            case R.id.invitations:
                invitations.setBackgroundResource(R.mipmap.small_btn_pressed);
                scheduled.setBackgroundResource(R.mipmap.small_btn_normal);
                scheduled.setTextColor(Color.BLACK);
                invitations.setTextColor(Color.WHITE);
                seeInvitationState = true;
                listAdapter.setOnItemClickListener(this);
                listAdapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public void onItemClick(int position) {
        Intent goToInvitation = new Intent(this,Invitation.class);
        startActivity(goToInvitation);

    }



        private void createList () {
            AppointmentDetails appointment;
            for (int i = 0; i < 10; i++) {
                SimpleDateFormat sdf = new SimpleDateFormat(DATE_TIME);
                appointment = new AppointmentDetails();
                appointment.setAppointmentDate(sdf.format(new Date()));
                appointment.setPic(R.mipmap.small_placeholder);
                appointment.setName(TESTING + i);
                appointment.setDuration(TIME);
                appointment.setService(SERVICE + i);
                appointment.setCustomerNotes(NOTES + i);
                appointment.setInvitationDate(INVITE + i);
                appointment.setLocation(CITY_CH);
                appointmentsArrayList.add(appointment);
                listAdapter.notifyDataSetChanged();
            }
        }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch(position)
        {
            case 6:
                logoutApiCall();

        }
    }
    void logoutApiCall()
    {
        String token=sharedPreferences.getString(ACCESS_TOKEN, "");
        new RestClient().getService().logout("Bearer " + token
                , new Callback<LoginResponse>() {
            @Override
            public void success(LoginResponse loginResponse, Response response) {
                startActivity(new Intent(Appointments.this, SignIn.class));
                finish();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(Appointments.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
