package com.example.mukesh.splash.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mukesh.splash.Utils.Constants;
import com.example.mukesh.splash.Adapters.GenderSpinnerAdapter;
import com.example.mukesh.splash.Entities.LoginResponse;
import com.example.mukesh.splash.Entities.MyResponse;
import com.example.mukesh.splash.R;
import com.example.mukesh.splash.Entities.RegionName;
import com.example.mukesh.splash.Adapters.RegionSpinnerAdapter;
import com.example.mukesh.splash.Retrofit.RestClient;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedString;

public class SignUp extends AppCompatActivity implements View.OnClickListener, Constants {
    Button signUp;
    ImageButton back;
    ArrayList<RegionName> regions;
    Spinner regionSpinner, genderSpinner;
    RegionSpinnerAdapter regionSpinnerAdapter;
    GenderSpinnerAdapter genderSpinnerAdapter;
    EditText firstName, lastName, email, mobileNum, password, confirmPassword, empId;
    TypedString fName, lName, eMail, mNum, passWord, confirmPass, eId, region, gender;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        firstName = (EditText) findViewById(R.id.First);
        lastName = (EditText) findViewById(R.id.Last);
        email = (EditText) findViewById(R.id.email1);
        mobileNum = (EditText) findViewById(R.id.Mobile);
        password = (EditText) findViewById(R.id.password1);
        confirmPassword = (EditText) findViewById(R.id.Confirm);
        empId = (EditText) findViewById(R.id.Employee);

        back = (ImageButton) findViewById(R.id.back);
        signUp = (Button) findViewById(R.id.signUp);
        TextView textView = (TextView) findViewById(R.id.text3);
        String text = "<font color=#ABACAD>Already a Member? </font>" +
                " <font color=#CD2E44><u>SIGN IN</u></font>";
        textView.setText(Html.fromHtml(text));
        TextView textView1 = (TextView) findViewById(R.id.text4);
        TextView header = (TextView) findViewById(R.id.default_text);
        header.setText(SIGN_UP);
        String text1 = "<font color=#ABACAD>Do you agree to </font>" +
                " <font color=#CD2E44><u>Terms of Service and Privacy?</u></font>";
        textView1.setText(Html.fromHtml(text1));
        textView1.setOnClickListener(this);
        textView.setOnClickListener(this);
        signUp.setOnClickListener(this);
        back.setOnClickListener(this);
        regions = new ArrayList<>();
        RegionName regionName = new RegionName();
        regionName.setRegionName(REGION);
        regions.add(regionName);
        genderSpinner = (Spinner) findViewById(R.id.spinner);
        genderSpinnerAdapter = new GenderSpinnerAdapter(this, getResources().getStringArray(R.array.Gender));
        genderSpinner.setAdapter(genderSpinnerAdapter);
        regionSpinner = (Spinner) findViewById(R.id.spinner1);
        regionSpinnerAdapter = new RegionSpinnerAdapter(this, regions);
        regionSpinner.setAdapter(regionSpinnerAdapter);

        new RestClient().getService().getCountryCodeServerCall(new Callback<MyResponse>() {
            @Override
            public void success(MyResponse myResponse, Response response) {
 /*               String s = new Gson().toJson(myResponse);
                Toast.makeText(SignUp.this, s, Toast.LENGTH_SHORT).show();*/
                regions.addAll(myResponse.getData());
                regionSpinnerAdapter.notifyDataSetChanged();
//                tvResult.setText(s);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(SignUp.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signUp:
                ProgressDialog progressDialog = new ProgressDialog(SignUp.this);
                progressDialog.setMessage(PROCESSING);
                progressDialog.show();
                fName = new TypedString(firstName.getText().toString());
                lName = new TypedString(lastName.getText().toString());
                eMail = new TypedString(email.getText().toString());
                mNum = new TypedString(mobileNum.getText().toString());
                passWord = new TypedString(password.getText().toString());
                confirmPass = new TypedString(confirmPassword.getText().toString());
                eId = new TypedString(empId.getText().toString());
                gender = new TypedString(genderSpinner.getSelectedItem().toString());
                region = new TypedString(regionSpinner.getSelectedItem().toString());
                retrofitCall();
                progressDialog.dismiss();
                // Intent intent3 = new Intent(this, AboutMe.class);
                //startActivity(intent3);
                break;
            case R.id.text3:
                Intent intent4 = new Intent(this, SignIn.class);
                startActivity(intent4);
                break;
            case R.id.text4:
                Intent intent5 = new Intent(this, SignIn.class);
                startActivity(intent5);
                break;
            case R.id.back:
                Intent intent6 = new Intent(this, SignIn.class);
                startActivity(intent6);
                break;

        }
    }

    public void retrofitCall() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(SignUp.this);
        String deviceToken = sharedPreferences.getString(DEVICE_TOKEN, "");
        new RestClient().getService().getRegister(fName, lName,
                eMail, eId, region,
                passWord, mNum, new TypedString(DEVICE_TYPE.toString()),
                new TypedString(deviceToken.toString()), gender, new Callback<LoginResponse>() {
                    @Override
                    public void success(LoginResponse registerResponse, Response response) {
                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(SignUp.this);
                        SharedPreferences.Editor sharedEditor = sharedPreferences.edit();
                        sharedEditor.putString(ACCESS_TOKEN, registerResponse.getData().getToken().toString());
                        sharedEditor.putString("firstName",registerResponse.getData().getAgent().getName().getFirstName());
                        sharedEditor.commit();
                        Intent goToAboutMe = new Intent(SignUp.this,AboutMe.class);
                        startActivity(goToAboutMe);
                        Toast.makeText(SignUp.this, SUCCESS + registerResponse.getStatusCode() + " , " + response.getStatus(), Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(SignUp.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}


