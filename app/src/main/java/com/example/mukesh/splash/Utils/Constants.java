package com.example.mukesh.splash.Utils;

/**
 * Created by Mukesh on 11/3/2015.
 */
public interface Constants {
    int SPLASH_DISPLAY_LENGTH = 2000;
    String ABOUT_ME = "ABOUT ME";
    String ADDRESS = "ADDRESS";
    int MAX_LENGTH = 140;
    String APPOINTMENT = "APPOINTMENTS";
    String INVITATION_DETAILS = "INVITATION DETAILS";
    String CUSTOMER_LOCATION = "CUSTOMER LOCATION";
    String LATLNG = "LatLng";
    String EMPTY =  "Unable to find address";
    String BEARER = "Bearer ";
    String LOCATION ="location";
    String NAME = "name";
    String GCM = "GCM";
    String PROJECT ="735761420460";
    String NIL= "";
    String UNABLE = "Unable to find address";
    String REGION= "Region";
    String SIGN_UP = "SignUp";
    String SUCCESS = "Success";
    String DATE_TIME = "h:mm a, MMMM dd,yyyy";
    String TESTING = "Testing :- ";
    String TIME = "60 min";
    String SERVICE = "Temp Service ";
    String NOTES ="Empty Notes ";
    String INVITE ="temp invite ";
    String CITY_CH ="Chandigarh";
    String KEY = "?key=";
    String INPUT ="&input=";
    String UT ="utf8";
    String BASE = "http://reddoorspa.clicklabs.in:8000";
    String EXCEPTION = "Exception";
    String GEOCODER_BAD_REQUEST="Unable to connect to Geocoder";
    String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    String TYPE_AUTOCOMPLETE = "/autocomplete";
    String OUT_JSON = "/json";
    String API_KEY = "AIzaSyC-M-ymiKvbXLYuixc-wSM79MiVOXJceEM";
    String ERROR_PROCESSING_URL = "Error processing Places API URL";
    String ERROR_CONNECTING_PLACES =  "Error connecting to Places API";
    String PREDICTIONS = "Predictions";
    String DESCRIPTIONS = "Descriptions";
    String JSON_ERROR= "Cannot process JSON results";
    String EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z]).{6,})";
    String FIRST_NAME_PATTERN = "([a-zA-Z]*.{3,})";
    String LAST_NAME_PATTERN = "([a-zA-z]*.{3,})";
    String MSG_NO_INTERNET_CONNECTION = "Check Internet connection";
    String SELECT_LOCATION = "SELECT LOCATION";
    String DEVICE_TOKEN = "Device token";
    String ACCESS_TOKEN = "Access token";
    String DEVICE_TYPE = "ANDROID";
    String FILL_UP = "All field are to be filled";
    String PROCESSING ="PROCESSING";
    String ZIP = "zip",CITY = "city",STATE = "state",STREET = "street",APPARTMENT = "appartment";


}
