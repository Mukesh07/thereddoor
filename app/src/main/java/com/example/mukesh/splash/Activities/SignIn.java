package com.example.mukesh.splash.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mukesh.splash.Utils.Constants;
import com.example.mukesh.splash.Entities.LoginResponse;
import com.example.mukesh.splash.R;
import com.example.mukesh.splash.Retrofit.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SignIn extends AppCompatActivity implements View.OnClickListener, Constants {
    Button signIn;
    ImageButton back;
    String mEmail, mPassword;
    EditText email, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        signIn = (Button) findViewById(R.id.signIn);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        signIn.setOnClickListener(this);
        back = (ImageButton) findViewById(R.id.back);
        TextView textView = (TextView) findViewById(R.id.text2);
        String text = "<font color=#ABACAD>Not Registered yet? </font>" +
                " <font color=#CD2E44><u>SIGN UP</u></font>" +
                "<font color=#ABACAD> here</font>";
        textView.setText(Html.fromHtml(text));
        textView.setOnClickListener(this);
        back.setOnClickListener(this);
    }

    public void getLoginCall() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(SignIn.this);
        String deviceToken = sharedPreferences.getString(DEVICE_TOKEN, "");
        new RestClient().getService().getLogin(mEmail, mPassword,
                DEVICE_TYPE,
                deviceToken, new Callback<LoginResponse>() {
                    @Override
                    public void success(LoginResponse registerResponse, Response response) {

                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(SignIn.this);
                        SharedPreferences.Editor sharedEditor = sharedPreferences.edit();
                        sharedEditor.putString(ACCESS_TOKEN, registerResponse.getData().getToken().toString());
                        sharedEditor.putString("firstName",registerResponse.getData().getAgent().getName().getFirstName());
                        sharedEditor.commit();
                        Intent intent1 = new Intent(SignIn.this, Appointments.class);
                        startActivity(intent1);
//                        Toast.makeText(SignIn.this, "Success" + registerResponse.getStatusCode() + " , " + response.getStatus(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(SignIn.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signIn:
                ProgressDialog progressDialog = new ProgressDialog(SignIn.this);
                progressDialog.setMessage(PROCESSING);
                progressDialog.show();
                mEmail = email.getText().toString();
                mPassword = password.getText().toString();
                getLoginCall();
                progressDialog.dismiss();
                break;
            case R.id.text2:
                Intent intent2 = new Intent(this, SignUp.class);
                startActivity(intent2);
                break;
            case R.id.back:
                ActivityCompat.finishAffinity(this);
                break;

        }
    }
}