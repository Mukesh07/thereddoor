package com.example.mukesh.splash.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mukesh.splash.Entities.LoginResponse;
import com.example.mukesh.splash.R;
import com.example.mukesh.splash.Retrofit.RestClient;
import com.example.mukesh.splash.Utils.Constants;
import com.example.mukesh.splash.Utils.ConvertLocationToLatLng;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class Address extends AppCompatActivity implements View.OnClickListener,Constants {

    EditText street, apartment, city, state, zip;
    double latitude, longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        Button save;
        ImageButton back;
        RelativeLayout search;
        street = (EditText) findViewById(R.id.street);
        apartment = (EditText) findViewById(R.id.suite);
        city = (EditText) findViewById(R.id.city);
        state = (EditText) findViewById(R.id.state);
        zip = (EditText) findViewById(R.id.zip);
        save = (Button) findViewById(R.id.save1);
        search = (RelativeLayout) findViewById(R.id.relative1);
        back = (ImageButton) findViewById(R.id.back);
        TextView header = (TextView) findViewById(R.id.default_text);
        header.setText(ADDRESS);
        save.setOnClickListener(this);
        search.setOnClickListener(this);
        back.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save1:
                String zipCode = zip.getText().toString();
                String cityName = city.getText().toString();
                String streetAddress = street.getText().toString();
                String stateName = state.getText().toString();
                String apartmentNo = apartment.getText().toString();
                if (zipCode != null && !zipCode.isEmpty()
                        && cityName != null && !cityName.isEmpty()
                        && streetAddress != null && !streetAddress.isEmpty()
                        && stateName != null && !stateName.isEmpty()
                        && apartmentNo != null && !apartmentNo.isEmpty()) {
                    if (latitude == 0.0 || longitude == 0.0) {
                        String agentAddress = apartmentNo + " " + streetAddress + " " + cityName + " " + stateName;
                        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
                        if (status != ConnectionResult.SUCCESS) {
                            Toast.makeText(Address.this, MSG_NO_INTERNET_CONNECTION, Toast.LENGTH_SHORT).show();
                        } else {
                            ConvertLocationToLatLng convertLocationToLatLng = new ConvertLocationToLatLng();
                            convertLocationToLatLng.getLatLng(agentAddress, getApplicationContext(), new LatLngHandler());
                        }
                    } else {

                        addressApiCall();
                    }
                } else
                    Toast.makeText(Address.this, FILL_UP, Toast.LENGTH_SHORT).show();

                break;
            case R.id.relative1:
                Intent intent9 = new Intent(this, SelectLocation.class);
                startActivityForResult(intent9, 1);
                break;
            case R.id.back:
                Intent intent10 = new Intent(this, AboutMe.class);
                startActivity(intent10);
                break;


        }
    }

    public boolean googleMapAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status)
            return true;
        else
            return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            String address = data.getStringExtra("address");
            if (!googleMapAvailable()) {
                Toast.makeText(Address.this, MSG_NO_INTERNET_CONNECTION, Toast.LENGTH_SHORT).show();

            } else {

                ConvertLocationToLatLng convertLocationToLatLng = new ConvertLocationToLatLng();
                convertLocationToLatLng.getAddressDetails(address, getApplicationContext(), new AddressDetailsHandler());
            }
        }
    }


    public void addressApiCall() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String token = sharedPreferences.getString(ACCESS_TOKEN, "");
        new RestClient().getService().postAddress("Bearer " + token, street.getText().toString(),
                apartment.getText().toString(), city.getText().toString(), state.getText().toString(),
                zip.getText().toString(), latitude, longitude
                , new Callback<LoginResponse>() {
                    @Override
                    public void success(LoginResponse loginRegisterResponse, Response response) {
                        startActivity(new Intent(Address.this, Appointments.class));
                        finish();

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(Address.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }
        private class AddressDetailsHandler extends android.os.Handler {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 1:
                        Bundle bundle = msg.getData();
                        city.setText(bundle.getString(CITY));
                        zip.setText(bundle.getString(ZIP));
                        state.setText(bundle.getString(STATE));
                        street.setText(bundle.getString(STREET));
                        double[] latLong;
                        latLong = bundle.getDoubleArray("AddressLatLng");
                        latitude = latLong[0];
                        longitude = latLong[1];
                        break;
                }
            }

        }


    private class LatLngHandler extends android.os.Handler {
        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            double[] latLong = bundle.getDoubleArray(LATLNG);

            switch (msg.what) {
                case 1:
                    latitude = latLong[0];
                    longitude = latLong[1];
                    addressApiCall();
                    break;

            }
        }
    }
}