package com.example.mukesh.splash.Entities;

import java.util.ArrayList;

/**
 * Created by Mukesh on 11/2/2015.
 */
public class RegionResponse {
    String message;
    ArrayList<RegionName> data;

    public ArrayList<RegionName> getData() {
        return data;
    }

    public void setData(ArrayList<RegionName> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}