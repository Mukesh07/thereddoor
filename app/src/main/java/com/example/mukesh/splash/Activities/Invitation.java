package com.example.mukesh.splash.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mukesh.splash.R;
import com.example.mukesh.splash.Utils.Constants;

public class Invitation extends AppCompatActivity implements View.OnClickListener,Constants {
    ImageButton back;
    TextView customerName;
    ImageView map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invitation);
        TextView header = (TextView) findViewById(R.id.default_text);
        header.setText(INVITATION_DETAILS);
        map=(ImageView)findViewById(R.id.map);
        String name = getIntent().getStringExtra(NAME);
        customerName = (TextView)findViewById(R.id.name);
        customerName.setText(name);
        back = (ImageButton) findViewById(R.id.back);
        back.setOnClickListener(this);
        map.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
           super.onBackPressed();
                break;

        }
    }
}