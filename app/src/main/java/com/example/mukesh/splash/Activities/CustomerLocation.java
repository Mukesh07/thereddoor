package com.example.mukesh.splash.Activities;

import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mukesh.splash.Utils.Constants;
import com.example.mukesh.splash.Utils.ConvertLocationToLatLng;
import com.example.mukesh.splash.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class CustomerLocation extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener,Constants {
    TextView header;
    ImageButton backButton;
    double[] latlng = new double[2];
    GoogleMap googleMap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_location);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        googleMap=mapFragment.getMap();
        header = (TextView)findViewById(R.id.default_text);
        header.setText(CUSTOMER_LOCATION);
        backButton = (ImageButton)findViewById(R.id.back);
        backButton.setOnClickListener(this);
      /*  if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

        } else {
            // Show rationale and request permission.
        }
      */  if(!googleMapAvailable())
        {
            Toast.makeText(this, MSG_NO_INTERNET_CONNECTION, Toast.LENGTH_SHORT).show();

        }
        else
        {
            String customerLocation = getIntent().getStringExtra(LOCATION);
            ConvertLocationToLatLng convertLocationToLatLng = new ConvertLocationToLatLng();
            convertLocationToLatLng.getLatLng(customerLocation,getApplicationContext(),new LatLngHandler());
        }
    }


    @Override
    public void onMapReady(GoogleMap map) {

    }
    private void setLocation() {
        LatLng latLng = new LatLng(latlng[0], latlng[1]);
        googleMap.addMarker(new MarkerOptions().position(latLng));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(5));
    }
    @Override
    public void onClick(View v) {
        super.onBackPressed();
    }

    public boolean googleMapAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if(ConnectionResult.SUCCESS == status)
            return true;
        else
            return false;
    }

    private class LatLngHandler extends android.os.Handler{
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    Bundle bundle = msg.getData();
                    latlng = bundle.getDoubleArray(LATLNG);
                    setLocation();
                    break;
                default:
                    Toast.makeText(CustomerLocation.this, UNABLE, Toast.LENGTH_SHORT).show();
            }
        }
    }
}