package com.example.mukesh.splash.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mukesh.splash.Utils.Constants;
import com.example.mukesh.splash.Entities.LoginResponse;
import com.example.mukesh.splash.R;
import com.example.mukesh.splash.Retrofit.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AboutMe extends AppCompatActivity implements View.OnClickListener, Constants {
    Button save;
    ImageButton back;
    EditText characterCount;
    TextView number_of_character;
    String aboutData;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_me);
        characterCount = (EditText) findViewById(R.id.about);
        number_of_character = (TextView) findViewById(R.id.count);
        characterCount.addTextChangedListener(mTextEditorWatcher);

        save = (Button) findViewById(R.id.save);
        save.setOnClickListener(this);
        TextView header = (TextView) findViewById(R.id.default_text);
        header.setText(ABOUT_ME);
        back = (ImageButton) findViewById(R.id.back);
        back.setOnClickListener(this);


    }

    private final TextWatcher mTextEditorWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            number_of_character.setText(String.valueOf(s.length()) + "|" + MAX_LENGTH);
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save:
                progressDialog = new ProgressDialog(AboutMe.this);
                progressDialog.setMessage(PROCESSING);
                progressDialog.show();
                aboutData = characterCount.getText().toString();
                if (!aboutData.isEmpty()) {
                    retrofitCall();
                } else {
                    Toast.makeText(this, EMPTY, Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
                break;

            case R.id.back:
                Intent intent6 = new Intent(this, SignIn.class);
                startActivity(intent6);
                break;
        }
    }

    public void retrofitCall() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(AboutMe.this);
        String token = sharedPreferences.getString(ACCESS_TOKEN, "");
        new RestClient().getService().putAboutMe(BEARER + token, aboutData.toString(), new Callback<LoginResponse>() {
            @Override
            public void success(LoginResponse loginResponse, Response response) {

                Intent goToAddress = new Intent(AboutMe.this, Address.class);
                startActivity(goToAddress);
                finish();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(AboutMe.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}