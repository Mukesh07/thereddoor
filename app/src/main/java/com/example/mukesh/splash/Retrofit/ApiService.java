package com.example.mukesh.splash.Retrofit;

import com.example.mukesh.splash.Entities.LoginResponse;
import com.example.mukesh.splash.Entities.MyResponse;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.mime.TypedString;

/**
 * Created by Mukesh on 11/3/2015.
 */
public interface ApiService {
    String GET_REGION = "/api/agent/region";
    String POST_LOGIN = "/api/agent/login";
    String PUT_ABOUT_ME = "/api/agent/aboutme";
    String PUT_LOGOUT = "/api/agent/logout";
    String POST_REGISTER = "/api/agent/register";
    String POST_ADDRESS = "/api/agent/address";
    String GET_APPOINTMENTS = "/api/agent/yourAppointment";

    @GET(GET_REGION)
    void getCountryCodeServerCall(Callback<MyResponse> callback);

    @FormUrlEncoded
    @POST(POST_LOGIN)
    void getLogin(@Field("emailId") String email,
                  @Field("password") String password,
                  @Field("deviceType") String deviceType,
                  @Field("deviceToken") String deviceToken,
                  Callback<LoginResponse> callback);
    @PUT(PUT_LOGOUT)
    void logout(@Header("authorization") String authorization, Callback<LoginResponse> callback);

    @FormUrlEncoded
    @PUT(PUT_ABOUT_ME)
    void putAboutMe(@Header("authorization") String authorization,
                    @Field("aboutMe") String aboutMe,
                    Callback<LoginResponse> callback);

    @Multipart
    @POST(POST_REGISTER)
    void getRegister(@Part("firstName") TypedString firstName,
                     @Part("lastName") TypedString lastName,
                     @Part("emailId") TypedString emailId,
                     @Part("employeeId") TypedString employeeId,
                     @Part("region") TypedString region,
                     @Part("password") TypedString password,
                     @Part("phoneNumber") TypedString phoneNumber,
                     @Part("deviceType") TypedString deviceType,
                     @Part("deviceToken") TypedString deviceToken,
                     @Part("gender") TypedString gender,
                     Callback<LoginResponse> callback);
    @FormUrlEncoded
    @POST(POST_ADDRESS)
    void postAddress(@Header("authorization") String authorization,
                     @Field("streetAddress") String streetAddress,
                     @Field("apartment") String apartment,
                     @Field("city") String city,
                     @Field("state") String state,
                     @Field("zip") String zip,
                     @Field("latitude") double latitude,
                     @Field("longitude") double longitude,
                     Callback<LoginResponse> callback);



}


