package com.example.mukesh.splash.Activities;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Location;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mukesh.splash.Utils.Constants;
import com.example.mukesh.splash.Utils.ConvertLocationToLatLng;
import com.example.mukesh.splash.R;
import com.example.mukesh.splash.Adapters.SelectLocationAdapter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


/**
 * Created by Mukesh on 11/3/2015.
 */
public class SelectLocation extends FragmentActivity implements
        OnMapReadyCallback, View.OnClickListener, Constants, AdapterView.OnItemClickListener, GoogleMap.OnMyLocationChangeListener {
    TextView header;
    EditText chooseLocation;
    Button done;
    ImageButton backButton;
    GoogleMap googleMap;
    boolean locationSelected;
    AutoCompleteTextView autoCompView;
    double[] latlng;
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        header = (TextView) findViewById(R.id.default_text);
        chooseLocation = (EditText) findViewById(R.id.chooseLocation);
        done = (Button) findViewById(R.id.done);
        backButton = (ImageButton) findViewById(R.id.back);
        header.setText(SELECT_LOCATION);
        done.setOnClickListener(this);
        backButton.setOnClickListener(this);
        autoCompView = (AutoCompleteTextView) findViewById(R.id.chooseLocation);
        autoCompView.setAdapter(new SelectLocationAdapter(this, R.layout.list_view));
        autoCompView.setOnItemClickListener(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        latlng = new double[2];
        googleMap = mapFragment.getMap();
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());
        if (status != ConnectionResult.SUCCESS) {
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
        } else {
            SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            googleMap = fm.getMap();
            googleMap.setMyLocationEnabled(true);
            googleMap.setOnMyLocationChangeListener(this);
        }
        ImageView locationButton = (ImageView) ((View) mapFragment.getView().findViewById(1).getParent()).findViewById(2);
        locationButton.setImageResource(R.drawable.selector);


    }

    private void setLocation() {
        googleMap.clear();
        LatLng latLng = new LatLng(latlng[0], latlng[1]);
        googleMap.addMarker(new MarkerOptions().position(latLng).anchor(0.5f, 0.5f).icon(BitmapDescriptorFactory.fromResource(R.mipmap.your_location_icon)));
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
    }

    public boolean googleMapAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status)
            return true;
        else
            return false;
    }

    private class LatLngHandler extends android.os.Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    Bundle bundle = msg.getData();
                    latlng = bundle.getDoubleArray(LATLNG);
                    locationSelected = true;
                    setLocation();
                    break;
                default:
                    Toast.makeText(SelectLocation.this, EMPTY, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                super.onBackPressed();
                break;

            case R.id.done:
                progressDialog = new ProgressDialog(SelectLocation.this);
                progressDialog.setMessage(PROCESSING);
                progressDialog.show();
                Intent intent = new Intent();
                intent.putExtra("address", autoCompView.getText().toString());
                setResult(RESULT_OK, intent);
                finish();
                progressDialog.dismiss();
                break;
        }

    }

    public void onItemClick(AdapterView adapterView, View view, int position, long id) {
        String agentAddress = (String) adapterView.getItemAtPosition(position);
        if (!googleMapAvailable()) {
            Toast.makeText(SelectLocation.this, MSG_NO_INTERNET_CONNECTION, Toast.LENGTH_SHORT).show();

        } else {

            ConvertLocationToLatLng convertLocationToLatLng = new ConvertLocationToLatLng();
            convertLocationToLatLng.getLatLng(agentAddress, getApplicationContext(), new LatLngHandler());
        }
        Toast.makeText(this, agentAddress, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onMyLocationChange(Location location) {
        if (!locationSelected) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            LatLng latLng = new LatLng(latitude, longitude);
//            googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
//            googleMap.addMarker(new MarkerOptions().position(latLng));
        }
    }


}