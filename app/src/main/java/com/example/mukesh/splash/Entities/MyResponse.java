package com.example.mukesh.splash.Entities;

import com.example.mukesh.splash.Entities.RegionName;

import java.util.ArrayList;

/**
 * Created by Mukesh on 11/3/2015.
 */
public class MyResponse {
    String message;
    ArrayList<RegionName> data;


    public String getMessage() {
        return message;
    }

    public ArrayList<RegionName> getData() {
        return data;
    }

    public void setData(ArrayList<RegionName> data) {
        this.data = data;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
