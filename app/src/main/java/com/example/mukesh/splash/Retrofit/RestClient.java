package com.example.mukesh.splash.Retrofit;
import com.example.mukesh.splash.Utils.Constants;
import com.squareup.okhttp.OkHttpClient;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by Mukesh on 11/3/2015.
 */


public class RestClient implements Constants{
    ApiService service;
    final String BASE_URL = BASE;

    public RestClient() {
        OkHttpClient okHttpClient = new OkHttpClient();
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .setClient(new OkClient(okHttpClient))
                .setLogLevel(RestAdapter.LogLevel.FULL);

        RestAdapter restAdapter = builder.build();
        service = restAdapter.create(ApiService.class);

    }

    public ApiService getService() {
        return service;
    }
}
