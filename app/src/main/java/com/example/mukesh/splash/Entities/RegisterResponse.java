package com.example.mukesh.splash.Entities;

public class RegisterResponse {
    String  statusCode;
    String  error;
    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }



}