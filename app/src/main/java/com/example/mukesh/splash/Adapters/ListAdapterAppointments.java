package com.example.mukesh.splash.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.mukesh.splash.Activities.Appointments;
import com.example.mukesh.splash.Activities.CustomerLocation;
import com.example.mukesh.splash.Entities.AppointmentDetails;
import com.example.mukesh.splash.Utils.Constants;
import com.example.mukesh.splash.Utils.OnItemClickListener;
import com.example.mukesh.splash.R;

import java.util.ArrayList;

/**
 * Created by Mukesh on 10/30/2015.
 */
public class ListAdapterAppointments extends BaseAdapter implements Constants {
    private ArrayList<AppointmentDetails> appointmentDetailsArrayList;
    private Context mContext;
    TextView tvName, tvService, tvDuration, tvLocation, tvTime, tvTimeLeft;
    ImageView ivPic;
    RelativeLayout invite;
    OnItemClickListener onItemClickListener;
    ImageView map;

    public ListAdapterAppointments(Context mcontext, ArrayList<AppointmentDetails> appointmentDetailsArrayList) {
        this.appointmentDetailsArrayList = appointmentDetailsArrayList;
        this.mContext = mcontext;
    }


    @Override
    public int getCount() {
        return appointmentDetailsArrayList.size();
    }

    @Override
    public AppointmentDetails getItem(int position) {
        return appointmentDetailsArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater li = LayoutInflater.from(mContext);
        convertView = li.inflate(R.layout.custom1, parent, false);
        tvName = (TextView) convertView.findViewById(R.id.name);
        tvService = (TextView) convertView.findViewById(R.id.Massage);
        tvDuration = (TextView) convertView.findViewById(R.id.tv_duration);
        invite = (RelativeLayout) convertView.findViewById(R.id.invite);
        map =(ImageView) convertView.findViewById(R.id.map);
        tvLocation = (TextView) convertView.findViewById(R.id.location);
        tvTime = (TextView) convertView.findViewById(R.id.tv_time_date);
        tvTimeLeft = (TextView) convertView.findViewById(R.id.tv_time_left);
        ivPic = (ImageView) convertView.findViewById(R.id.image_view_profile_pic);
        final AppointmentDetails appointmentDetails = appointmentDetailsArrayList.get(position);
        tvName.setText(appointmentDetails.getName());
        tvService.setText(appointmentDetails.getService());
        tvDuration.setText(appointmentDetails.getDuration());
        tvLocation.setText(appointmentDetails.getLocation());
        tvTime.setText(appointmentDetails.getAppointmentDate());
        tvTimeLeft.setText(appointmentDetails.getInvitationDate());
        ivPic.setImageResource(appointmentDetails.getPic());

        if (Appointments.seeInvitationState) {
            tvTimeLeft.setVisibility(View.VISIBLE);
            invite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(position);
                }
            });
        } else {
            tvTimeLeft.setVisibility(View.GONE);
            invite.setOnClickListener(null);

        }
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goCl = new Intent(mContext,CustomerLocation.class);
                goCl.putExtra(LOCATION,appointmentDetails.getLocation());
                mContext.startActivity(goCl);
            }
        });

        return convertView;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}